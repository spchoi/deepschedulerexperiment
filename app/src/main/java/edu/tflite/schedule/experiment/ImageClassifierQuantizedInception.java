package edu.tflite.schedule.experiment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.SpannableStringBuilder;
import android.util.Log;

import java.io.IOException;

public class ImageClassifierQuantizedInception extends ImageClassifier {
    private static final int IMAGE_MEAN = 128;
    private static final int IMAGE_STD = 128;
    private static int counter = 0;
    private int id = 0;

    private byte[][] labelProbArray = null;

    ImageClassifierQuantizedInception(Activity activity, int _id) throws IOException {
        super(activity);
        labelProbArray = new byte[1][getNumLabels()];
        id = _id;
    }

    @Override
    protected String getModelPath() {
        return "inception_v3_quant.tflite";
    }

    @Override
    protected String getLabelPath() {
        return "labels_imagenet_slim.txt";
    }

    @Override
    protected int getImageSizeX() {
        return 299;
    }

    @Override
    protected int getImageSizeY() {
        return 299;
    }

    @Override
    protected int getNumBytesPerChannel() {
        return 1;
    }

    @Override
    protected void addPixelValue(int pixelValue) {
        //maybe this part has some problems
        //byte bufferA = (byte)((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN)/IMAGE_STD);
        //float bufferAF = (float) ((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN)/IMAGE_STD);
        imgData.put((byte) ((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN)/IMAGE_STD));

        //byte bufferB = (byte)((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN)/IMAGE_STD);
        //float bufferBF = (float) ((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN)/IMAGE_STD);
        imgData.put((byte) ((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN)/IMAGE_STD));

        //byte bufferC = (byte)(((pixelValue & 0xFF) - IMAGE_MEAN)/IMAGE_STD);
        //float bufferCF = (float) (((pixelValue & 0xFF) - IMAGE_MEAN)/IMAGE_STD);
        imgData.put((byte) (((pixelValue & 0xFF) - IMAGE_MEAN)/IMAGE_STD));

        /*
        if(counter > 60) {
            Log.d("DEBUG!", "bufferA : " + bufferA + ", bufferAF : " + bufferAF);
            Log.d("DEBUG!", "bufferB : " + bufferB + ", bufferAF : " + bufferBF);
            Log.d("DEBUG!", "bufferC : " + bufferC + ", bufferAF : " + bufferCF);
            counter = 0;
        }else{
            counter = counter + 1;
        }
        */
    }

    @Override
    protected float getProbability(int labelIndex) {
        return labelProbArray[0][labelIndex];
    }

    @Override
    protected void setProbability(int labelIndex, Number value) {
        labelProbArray[0][labelIndex] = value.byteValue();
    }

    @Override
    protected float getNormalizedProbability(int labelIndex) {
        return (labelProbArray[0][labelIndex] & 0xff) / 255.0f;
    }

    @Override
    protected void runInference() {
        tflite.run(imgData, labelProbArray);
    }
}
