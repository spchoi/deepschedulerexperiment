package edu.tflite.schedule.experiment;

import android.os.Handler;
import android.os.HandlerThread;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.util.ArrayList;

public class Global {
    public static boolean QUANTIZED_TEST = true;
    public static final int TEST_AMOUNT = 6;
    public static final boolean FILE_WRITE = true;
    public static final Object lock = new Object();
    public static boolean runClassifier = false;
    public static TextView textView;
    public static ArrayList<HandlerThread> BackgroundThreads = null;
    public static ArrayList<Handler> BackgroundHandlers = null;
    public static ArrayList<ImageClassifier> ImageClassifiers = null;

    //idea1 counting
    //public static final Object count_lock = new Object();
    public static int[] InferenceCount = {0,0,0,0,0,0};

    //idea2 required-latency
    public static final Object count_lock_mkII = new Object();
    public static final Object infer_time_lock = new Object();
    public static long[] LastInferenceTime = {0,0,0,0,0,0};
    public static long[] Expected_InferenceTime = {0,0,0,0,0,0};


    public static FileOutputStream total_file = null;
    public static String formattedDate = null;
    public static String root = null;

    public static final long[] TEST_LATENCY =
            {2000,2000,2000,2000,2000,2000};

    //idea1
    public static int get_MinInfer_index(){
        //synchronized (count_lock) {
        int idx = 0;
        int minval = 9999999;
        for (int i = 0; i < TEST_AMOUNT; i++) {
            if (InferenceCount[i] < minval) {
                minval = InferenceCount[i];
                idx = i;
            }
        }
        return idx;
        //}
    }
    public static void update_infer_count(int idx){
        //synchronized (count_lock){
        InferenceCount[idx]++;
        //}
    }

    //idea2
    public static boolean can_i_progress(int idx, long required_latency, long current_time){
        //synchronized (count_lock_mkII) {
        long last_infertime = LastInferenceTime[idx];
        if(last_infertime+required_latency <= current_time){
            return true;
        }else{
            return false;
        }
        //}
    }
    public static void update_last_infertime (int idx, long last_infertime){
        LastInferenceTime[idx] = last_infertime;
    }

    //idea3
    public static void set_expected_infertime(int idx, long infertime){
        Expected_InferenceTime[idx] = infertime;
    }
    public static long get_expected_infertime(int idx){
        return Expected_InferenceTime[idx];
    }
    public static long get_last_infertime (int idx){
        //synchronized (count_lock_mkII){
        return LastInferenceTime[idx];
        //}
    }

}
