/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package edu.tflite.schedule.experiment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/** Main {@code Activity} class for the Camera app. */
public class CameraActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
      Log.e("DEBUG!", "On create!!!");
    super.onCreate(savedInstanceState);
      Calendar c = Calendar.getInstance();
      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
      Global.formattedDate = df.format(c.getTime());
      Global.root = Environment.getExternalStorageDirectory().toString();
      File myDir = new File(Global.root + "/sptest/"+Global.formattedDate+"/");
      if (!myDir.exists()) {
          myDir.mkdirs();
      }
      File file = new File (myDir, "Total.txt");
      if(Global.total_file == null) {
          try {
              Global.total_file = new FileOutputStream(file);
              Log.e("DEBUG!", "File open success!");
          }catch (Exception e){
              Log.e("DEBUG!", "File open failed!!");
              e.printStackTrace();
          }
      }
    setContentView(R.layout.activity_camera);
    if (null == savedInstanceState) {
      getFragmentManager()
          .beginTransaction()
          .replace(R.id.container, Camera2BasicFragment.newInstance())
          .commit();
    }

  }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            for(int i = 0; i< Global.ImageClassifiers.size(); i++){
                Global.ImageClassifiers.get(i).outputStream.flush();
                Global.ImageClassifiers.get(i).outputStream.close();
            }
            Global.total_file.close();
        }catch (Exception e){
            Log.d("DEBUG!", "File Close Failed!");
        }
    }
}
