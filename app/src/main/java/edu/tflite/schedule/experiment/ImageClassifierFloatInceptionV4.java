package edu.tflite.schedule.experiment;

import android.app.Activity;

import java.io.IOException;

public class ImageClassifierFloatInceptionV4 extends ImageClassifier {
    /**
     * Initializes an {@code ImageClassifier}.
     *
     * @param activity
     */

    private static final int IMAGE_MEAN = 128;
    private static final float IMAGE_STD = 128.0f;

    private float[][] labelProbArray = null;

    ImageClassifierFloatInceptionV4(Activity activity) throws IOException {
        super(activity);
        labelProbArray = new float[1][getNumLabels()];
    }

    @Override
    protected String getModelPath() {
        return "inception_v4.tflite";
    }

    @Override
    protected String getLabelPath() {
        return "labels_inception_v4.txt";
    }

    @Override
    protected int getImageSizeX() {
        return 299;
    }

    @Override
    protected int getImageSizeY() {
        return 299;
    }

    @Override
    protected int getNumBytesPerChannel() {
        return 4;
    }

    @Override
    protected void addPixelValue(int pixelValue) {
        imgData.putFloat((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
        imgData.putFloat((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
        imgData.putFloat(((pixelValue & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
    }

    @Override
    protected float getProbability(int labelIndex) {
        return labelProbArray[0][labelIndex];
    }

    @Override
    protected void setProbability(int labelIndex, Number value) {
        labelProbArray[0][labelIndex] = value.floatValue();
    }

    @Override
    protected float getNormalizedProbability(int labelIndex) {
        return getProbability(labelIndex);
    }

    @Override
    protected void runInference() {
        tflite.run(imgData, labelProbArray);
    }
}
