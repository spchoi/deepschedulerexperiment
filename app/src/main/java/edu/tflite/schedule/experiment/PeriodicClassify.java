package edu.tflite.schedule.experiment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.hardware.camera2.CameraDevice;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.TextureView;
import android.widget.TextView;

public class PeriodicClassify implements Runnable {
    ImageClassifier myIC;
    Handler myHD;
    Activity myAC;
    TextureView myTV;
    CameraDevice myCD;
    int myID;
    long required_latency;


    public PeriodicClassify(ImageClassifier _myIC, Handler _myHD, Activity _myAC, TextureView _myTV, CameraDevice _myCD, int id, long _latency){
        myIC = _myIC;
        myHD = _myHD;
        myAC = _myAC;
        myTV = _myTV;
        myCD = _myCD;
        myID = id;
        required_latency = _latency;
    }

    @Override
    public void run() {
        long delaytime;
        long currtime;
        long endtime = 0;
        boolean coldstart = false;
        boolean skipped = false;

        currtime = SystemClock.uptimeMillis();
        if (Global.runClassifier) {
             if( Global.get_expected_infertime(myID) != 0 &&
                    Global.get_last_infertime(myID) != 0){
                 coldstart = false;
                 long target_infer_start_time = Global.get_last_infertime(myID)+required_latency-Global.get_expected_infertime(myID);
                 if(target_infer_start_time > currtime){
                    skipped = true;
                 }else{
                     //do inference!
                     skipped = false;
                     long expected_finished_time = currtime + Global.get_expected_infertime(myID);
                     long required_finished_time = Global.get_last_infertime(myID) + required_latency;
                     if(expected_finished_time > required_finished_time + 500){
                         Log.e("WARNING", "model ID : " + myID + " EXP : " + expected_finished_time + " REQ : " + required_finished_time);
                     }

                     try {
                         DeepClassifyFrame();
                     } catch (Exception e) {

                     } finally {
                         Log.e("DEBUG!","Model Inference! model : " + myID);
                         endtime = SystemClock.uptimeMillis();
                         Global.update_last_infertime(myID, endtime);
                     }
                 }
            } else{
                //cold start
                coldstart = true;
                try {
                    DeepClassifyFrame();
                } catch (Exception e) {

                } finally {
                    endtime = SystemClock.uptimeMillis();
                    Global.update_last_infertime(myID, endtime);
                }
            }

        }

        if(coldstart == false && skipped == true){
            long target_start_infertime = Global.get_last_infertime(myID)+required_latency-Global.get_expected_infertime(myID);
            delaytime = target_start_infertime - currtime;
            Log.e("DEBUG!", "model : " + myID + " will delayed " + delaytime);
            myHD.postDelayed(this, delaytime);
        }else{
            if(coldstart == true){
                Log.e("DEBUG!", "cold_start handled! model id : " + myID);
            }
            myHD.post(this);
        }
    }

    public void DeepClassifyFrame() {
        SpannableStringBuilder textToShow = new SpannableStringBuilder();
        Bitmap bitmap = myTV.getBitmap(myIC.getImageSizeX(), myIC.getImageSizeY());
        myIC.classifyFrame(bitmap, textToShow, myID);
        bitmap.recycle();
    }
}
