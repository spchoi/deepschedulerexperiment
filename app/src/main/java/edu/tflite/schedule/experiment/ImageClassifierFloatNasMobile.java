package edu.tflite.schedule.experiment;

import android.app.Activity;

import java.io.IOException;

public class ImageClassifierFloatNasMobile extends ImageClassifier {
    /**
     * Initializes an {@code ImageClassifier}.
     *
     * @param activity
     */

    private float[][] labelProbArray = null;

    ImageClassifierFloatNasMobile(Activity activity) throws IOException {
        super(activity);
        labelProbArray = new float[1][getNumLabels()];
    }

    @Override
    protected String getModelPath() {
        return "nasnet_mobile.tflite";
    }

    @Override
    protected String getLabelPath() {
        return "labels_nas_mobile.txt";
    }

    @Override
    protected int getImageSizeX() {
        return 224;
    }

    @Override
    protected int getImageSizeY() {
        return 224;
    }

    @Override
    protected int getNumBytesPerChannel() {
        return 4;
    }

    @Override
    protected void addPixelValue(int pixelValue) {
        imgData.putFloat(((pixelValue >> 16) & 0xFF));
        imgData.putFloat(((pixelValue >> 8) & 0xFF));
        imgData.putFloat((pixelValue & 0xFF));
    }

    @Override
    protected float getProbability(int labelIndex) {
        return labelProbArray[0][labelIndex];
    }

    @Override
    protected void setProbability(int labelIndex, Number value) {
        labelProbArray[0][labelIndex] = value.floatValue();
    }

    @Override
    protected float getNormalizedProbability(int labelIndex) {
        return getProbability(labelIndex);
    }

    @Override
    protected void runInference() {
        tflite.run(imgData, labelProbArray);
    }
}
